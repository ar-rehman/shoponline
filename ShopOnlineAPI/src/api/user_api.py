import functools

from flask import (
    Blueprint, flash, g, redirect, render_template, request, session, url_for, jsonify
)
from werkzeug.security import check_password_hash, generate_password_hash

from sms_app.database.db import db_session
from sms_app.models.User import User
from sms_app.models.ModelHelper import ModelHelper

user_bp = Blueprint('user', __name__, url_prefix='/user',static_folder="../../static/dist",template_folder="../../static")


@user_bp.route('/', methods=['GET'], defaults={'path': ''})
@user_bp.route('/<path:path>', methods=['GET'])
def catch_all():
    return render_template("index.html")


@user_bp.route("/create_user",methods=['POST'])
def create_user():
    if(request.method == "GET"):
        return catch_all()
    username = request.form.get('apiusername')
    password = request.form.get('apipassword')
    error = None
    if not username:
        error = 'Username is required.'
    elif not password:
        error = 'Password is required.'
    else:
        u = User(username,password,0)
        db_session.add(u)
        db_session.commit()
        return "success"
    return error



@user_bp.route("/get_user",methods = ['POST'])
def get_user():
    if(request.method == "GET"):
        return catch_all()
    username = request.args.get('apiusername')
    error = None
    user = None
    if not username:
        return 'Username is required.'
    user = User.query.filter(User.name == username).first()
    if user is None:
        return "user doesnot exist"
    else:
        return jsonify(user.toDict())

@user_bp.route("/list_users")
def list_users():
    if(request.method == "GET"):
        return catch_all()
    list_users = ModelHelper.getUserAll()
    if list_users is None:
        return "user doesnot exist"
    else:
        return jsonify(list_users)

