
from sms_app.sms.SMSManager import SMSManager
from sms_app.models.User import User
import functools
from sqlalchemy import and_
from flask import (
    Blueprint, flash, g, redirect, render_template, request, session, url_for, jsonify
)
from werkzeug.security import check_password_hash, generate_password_hash




sms_bp = Blueprint('sms', __name__, url_prefix='/sms',static_folder="../../static/dist",template_folder="../../static")



@sms_bp.route('/', methods=['GET'], defaults={'path': ''})
@sms_bp.route('/<path:path>', methods=['GET'])
def catch_all(path):
    return render_template("index.html")



@sms_bp.route('/sendmsg')
def sendmsg():
    username = request.args.get('apiusername')
    password = request.args.get('apipassword')
    mobileno = request.args.get('mobileno')
    senderid = request.args.get('senderid')
    languagetype = request.args.get('languagetype')
    message = request.args.get('message')
    sm = SMSManager()
    sm.sendmsgs(message,mobileno)
    print("apiusername ",username)
    print("apipassword ", password)
    print("mobileno ", mobileno)
    print("senderid ", senderid)
    print("languagetype ", languagetype)
    print("message ", message)
    return '1'


@sms_bp.route('/credit')
def credit():
    username = request.args.get('apiusername')
    password = request.args.get('apipassword')
    if(not username or not password):
        return '0'
    u = User.query.filter(and_(User.name == username,User.password == password)).first()
    if(u==None):
        return '10'
    return str(u.credit)


