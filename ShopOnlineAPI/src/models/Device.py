from sqlalchemy import Column, Integer, String
from sqlalchemy.orm import relationship
from sms_app.database.db import Base

class Device(Base):
    __tablename__ = 'device'
    id = Column(Integer, primary_key=True,autoincrement=True)
    device_name = Column(String(50), unique=True)
    device_path = Column(String(50), unique=True)
    sim_type = Column(String(20))
    sim_no = Column(String(20),unique=True)
    device_imei = Column(Integer,unique=True)
    users = relationship("User",back_populates="device")


    def __init__(self, name=None, device_path=None, device_imei = None, sim_type = None, sim_no=None):
        self.device_name = name
        self.device_path = device_path
        self.device_imei = device_imei
        self.sim_type = sim_type
        self.sim_no = sim_no

    def __repr__(self):
        return '<Device %r>' % (self.device_path)

    def toDict(self):
        u = {
            "device_name":self.device_name,
            "device_path":self.device_path,
            "sim_type":self.sim_type,
            "sim_no":self.sim_no,
            "users":[user.toDict() for user in self.users]
        }


