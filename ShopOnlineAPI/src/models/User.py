from sqlalchemy import Column, Integer, String, ForeignKey
from sqlalchemy.orm import relationship
from sms_app.database.db import Base

class User(Base):
    __tablename__ = 'users'
    id = Column(Integer, primary_key=True,autoincrement=True)
    name = Column(String(50), unique=True)
    password = Column(String(120))
    credit = Column(Integer,default=0)
    device_id = Column(Integer,ForeignKey("device.id"))
    device=relationship("Device",back_populates="users")
    credit_history = relationship("CreditHistory",back_populates="user")


    def __init__(self, name=None, password=None, credit = None, device = None):
        self.name = name
        self.password = password
        self.credit = credit
        self.device = device

    def __repr__(self):
        return '<User %r>' % (self.name)

    def toDict(self):
        u = {"name":self.name,"credit":self.credit, "device":self.device[0].toDict()}


