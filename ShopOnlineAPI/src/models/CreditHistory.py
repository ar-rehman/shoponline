from sqlalchemy import Column, Integer, String, ForeignKey, Boolean, DateTime
from sms_app.database.db import Base
from sqlalchemy.orm import relationship
import datetime
import enum

class CreditHistory(Base):
    __tablename__ = 'credit_history'
    id = Column(Integer, primary_key=True,autoincrement=True)
    user_id = Column(Integer,ForeignKey("users.id"))
    credit = Column(Integer)
    credit_type = Column(Boolean)
    date = Column(DateTime,default=datetime.datetime.now)
    user=relationship("User",back_populates="credit_history",uselist=False)

    def __init__(self, userid, credit, credit_type):
        self.user_id = userid
        self.credit = credit
        self.credit_type = credit_type

    def __repr__(self):
        return '<CreditHistory %r %r>' % (self.credit,self.credit_type)

    def toDict(self):
        u = {"user_id":self.user_id,"credit":self.credit,"credit_type":self.credit_type, "date":self.date}



class CreditHistoryType(enum.Enum):

    Added = 0
    Removed = 1

