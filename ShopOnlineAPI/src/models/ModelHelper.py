from .User import User
from .CreditHistory import CreditHistory, CreditHistoryType
from sms_app.database.db import db_session
from sqlalchemy import and_

class ModelHelper:

    @staticmethod
    def getCreditHistoryByUser(userid,page_no=1,rows=10,order_by='date',order_type='desc',search_column=None,search_value=None):
        hist = CreditHistory.query.filter(CreditHistory.user_id == userid)
        hist = ModelHelper.applyfilters(hist, page_no, rows, order_by, order_type, search_column,search_value)
        hist_res = hist.all()
        return [ h.toDict() for h in hist_res]

    @staticmethod
    def getCreditHistoryAll():
        CreditHistory.query.order_by(CreditHistory.date.desc())

    @staticmethod
    def applyfilters(query,page_no=1,rows=10,order_by='date',order_type='desc',search_column=None,search_value=None):
        start, end = rows*page_no-1 , rows*page_no
        query = query.slice(start,end)
        if(order_type == "asc"):
            query = query.order_by(User[order_by].asc())
        else:
            query = query.order_by(User[order_by].desc())
        if search_column and search_value:
            query.filter(getattr(User,search_column) == search_value)

        return query

    @staticmethod
    def getUserAll():
        cursor_users = User.query.all()
        list_users = []
        for user in cursor_users:
            list_users.append(user.toDict())
        return list_users


