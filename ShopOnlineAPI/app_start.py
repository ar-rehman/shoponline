from flask import Flask
import os
from sms_app.api import sms_api, user_api, home_api
from sms_app.database.db import  init_db
from sms_app.common import Common
import atexit

app = Flask(__name__, instance_relative_config=True,static_folder="static/dist",template_folder="static")
app.config.from_mapping(
    SECRET_KEY = 'DEV')

app.register_blueprint(sms_api.sms_bp)
app.register_blueprint(user_api.user_bp)
app.register_blueprint(home_api.home_bp)



if __name__ == "__main__":
    port_num = Common.get_port()
    ip_addr = Common.get_ip()
    pub_ip_addr = Common.get_public_ip()
    Common.register_port(ip_addr,port_num)
    atexit.register(Common.unregister_port)
    init_db()
    print("public address is: http://"+pub_ip_addr+":"+str(port_num))
    app.run(port=port_num, host=ip_addr,debug=False)


